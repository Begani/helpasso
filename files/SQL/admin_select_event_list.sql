Select
event.id_event , event.nom_event, event.date_event_begin,  event.date_event_end, ville.ville,
event.adresse_event, contact.nom_contact, ville.code_postal
from event
INNER JOIN contact on contact.id_contact = event.id_contact
INNER JOIN ville on ville.id_ville = event.id_ville
GROUP BY event.id_event
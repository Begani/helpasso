<?php

require_once "inscription_admin_service.php";
/**
* Class Inscription_admin_envoi | fichier Inscription_admin_envoi.php
*
* Description de la classe à renseigner.
*
* Cette classe necessite l'utilisation de la classe :
*
*
* @package HelpAsso Project
* @subpackage service
* @author @Afpa Lab Team - Benjamin Montet stagiaire
* @copyright  1920-2080 The Afpa Lab Team Group Corporation World Company
* @version v1.0
*/

Class Inscription_admin_envoi	{
	
    /**
    * public $resultat is used to store all datas needed for HTML Templates
    * @var array
    */
    public $resultat;

    /**
    * init variables resultat
    *
    * execute main function
    */
    public function __construct() {
        $this->resultat = [];
        $this->main();
    }

    /**
    *
    * Destroy service
    *
    */
    public function __destruct() {
        // destroy objet_service
        unset($objet_service);
    }

    /**
    * Verify if password and mail match 
    * Yes : 
    */
    function main() {
        // Ici je fais mon appel $objet_service_new_admin->ma_methode_qui_est_dans_le_service
        $objet_service_new_admin = new Inscription_admin_service();

        if( isset($_POST['mdp_admin']) && isset($_POST['mdp_admin_repeat'])  && 
            isset($_POST['mail_admin']) && isset($_POST['mail_admin_repeat']) && 
            $_POST['mdp_admin'] == $_POST['mdp_admin_repeat'] &&
            $_POST['mail_admin'] == $_POST['mail_admin_repeat']
        ) 
        {

            $objet_service_new_admin -> login_create_admin();
            
        }
        else{
            echo ("Erreur") ;
        }

        // Je passe mes parametres pour y avoir acces dans mes pages HTML
        $this->resultat = $objet_service_new_admin->resultat;
        $this->VARS_HTML = $objet_service_new_admin->VARS_HTML;
    }
	
}

?>


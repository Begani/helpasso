<?php

require_once "fiche_event_service.php";
/**
* Class Fiche_event_selected | fichier fiche_event_selected.php
*
* Description de la classe à renseigner.
*
* Cette classe necessite l'utilisation de la classe :
*

* require_once "tournoi_service.php";
*
* @package HelpAsso Project
* @subpackage service
* @author @Afpa Lab Team - Benjamin Montet stagiaire
* @copyright  1920-2080 The Afpa Lab Team Group Corporation World Company
* @version v1.0
*/

Class Fiche_event_selected	{
	
    /**
    * public $resultat is used to store all datas needed for HTML Templates
    * @var array
    */
    public $resultat;

    /**
    * init variables resultat
    *
    * execute main function
    */
    public function __construct() {
        // init variables resultat
        $this->resultat = [];

        // execute main function
        $this->main();
    }

    /**
    *
    * Destroy service
    *
    */
    public function __destruct() {
        // destroy objet_service
        unset($objet_service);
    }

    /**
    * Get interface to gestion of inscrit_admin
    */
    function main() {
		
        $objet_service_tournoi_recherche = new Fiche_event_service();

        $objet_service_tournoi_recherche -> prepareEvent();

        $this->resultat = $objet_service_tournoi_recherche->resultat;
        $this->VARS_HTML = $objet_service_tournoi_recherche->VARS_HTML;
		
    
    }
}

?>


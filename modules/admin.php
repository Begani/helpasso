<?php
require_once "service.php";
require_once "admin_service.php";
/**
* Class Admin | file admin.php

* In this class, we have :
* Method main : instantiate classes Service and Admin_service and call construct_datalist Method from Admin_service

* List of classes needed for this class : 
* Admin_service
* Service

* @package HelpAsso Project
* @subpackage service.php, admin_service.php
* @author @HelpAsso - Benjamin Montet stagiaire
* @copyright 1920-2080 The Afpa Lab Team Group Corporation World Company
* @version v1.0
*/

Class Admin	{
	
    /**
    * public $resultat is used to store all datas needed for HTML Templates
    * @var array
    */
    public $resultat;
    /**
    * init parameter resultat
    */
    public function __construct() {
        // init parameter resultat
        $this->resultat = [];
        // execute main method
        $this->main();
    }

    /**
    *
    * Destroy service
    *
    */
    public function __destruct() {
        // destroy objet_service
        unset($objet_service);
    }

    /**
    * execute main method
    */
    function main() {

        $objet_service = new Service();
        $objet_service = new Admin_service();

        $objet_service -> construct_datalist();

        $this->resultat = $objet_service->resultat;
        $this->VARS_HTML = $objet_service->VARS_HTML;

    }
	
}

?>


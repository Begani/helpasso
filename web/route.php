<?php
	
	//SESSION START
	session_start();
	require "configuration.php";
	$GLOBALS_INI= Configuration::getGlobalsINI();
	
	// Include autoloader of Twig
	require_once $GLOBALS_INI["PATH_HOME"] . 'twig/vendor/autoload.php';

	// Class dynamic
	if ((isset($_GET["page"])) && ($_GET["page"] != ""))	{
		$monPHP= $_GET["page"];
	}  else  {
		if ((isset($_POST["page"])) && ($_POST["page"] != ""))	{
			$monPHP= $_POST["page"];
		}  else  {
			$monPHP= "index";
		}
	}
	error_log("page en paramètre : " . $monPHP);

	// Test if classes exist
	if (!(file_exists($GLOBALS_INI["PATH_HOME"] . $GLOBALS_INI["PATH_CLASS"] . $monPHP . ".php"))) {
		$monPHP= "accueil";
	}

	//Test if admin is connected to acces admin or inscription_admin interface
	if( ($monPHP == "admin" || $monPHP == "inscription_admin" ) && (!isset($_SESSION['admin']) || $_SESSION['admin'] == false )){
		$monPHP = "login";
	}

	$myClass= ucfirst($monPHP);
	
	require $GLOBALS_INI["PATH_HOME"] . $GLOBALS_INI["PATH_CLASS"] . $monPHP . ".php"; 

	$oMain= new $myClass();

	$page_to_load= "route.twig";
	//if AJAX WITH JSON
	if ((isset($oMain->VARS_HTML["bJSON"])) && ($oMain->VARS_HTML["bJSON"] == 1))	{
		$page_to_load= $monPHP . ".twig";
	}
	error_log("PageToLoad : " . $page_to_load);
	try {

		// Define the file where goes the templates
		$loader = new \Twig\Loader\FilesystemLoader($GLOBALS_INI["PATH_HOME"] . $GLOBALS_INI["PATH_FILES"]);
		// Initialize Twig
		$twig = new \Twig\Environment($loader);
		// Load the template
		$template = $twig->load($page_to_load);
		// Set variables and call the render
		echo $template->render(array(
			'resultat' => $oMain->resultat,
			'VARS_HTML' => $oMain->VARS_HTML,
			'GLOBALS_INI' => $GLOBALS_INI, 
			'session' => $_SESSION,
			'monPHP' => $monPHP,
		));
	} catch (Exception $e) {
		die ('ERROR: ' . $e->getMessage());
	}
	
	unset($oMain);
?> 

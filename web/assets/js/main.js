function sendmail()	{

    console.log(
        "nom : " + $('#nom').val() +
        " ---> prenom : " + $('#prenom').val() +
        " ---> mail : " + $('#mail').val() +
        " ---> modal_message : " + $('#modal_message').val()
    )

    var datas = {
        page : "sendmail",
        nom : $('#nom').val(),
        prenom : $('#prenom').val(),
        mail : $('#mail').val(),
        modal_message : $('#modal_message').val(),
        bJSON : 1
    }

    $.ajax({
        type: "POST",
        url: "route.php",
        async: false,
        data: datas,
        dataType: "json",
        cache: false,
    })
    .done(function(result) {
        setTimeout($('.modal-body').html('Message envoyé !'), 2000);
        $('#btn-mail-send').hide();
        $('#btn-cancel-mail').html("Retour");
        console.log(result)
    })
    .fail(function(err, result) {
        console.log(result);
        alert('error : ' + err);
    });
}


function convertDate(sDate)	{
    var aOfDates= sDate.split("-");
    return aOfDates[2] + "/" + aOfDates[1] + "/" + aOfDates[0];
}

/**
* Convert date jj/mm/aaaa into aaaa-mm-jj
*/
function inverseDate(sDate)	{
    var aOfDates= sDate.split("/");
    return aOfDates[2] + "-" + aOfDates[1] + "-" + aOfDates[0];
}
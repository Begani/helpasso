<?php

require_once "database.php";
require_once "security.php";
require_once "utils.php";

/**
* Class Initialize | file initialize.php
*
* In this class, we have 
* Method _construct which instantiates Database.php, Securite.php and Utils.php
* Method _destruct which close instances of Database.php, Securite.php and Utils.php

* List of classes needed for this class : 
* Initialize
* Security
* Utils

* @package HelpAsso Project
* @subpackage Initialize, Utils
* @author @Fansnhumans - Benjamin Montet Developper
* @copyright  2018-2020 The @Fansnhumans Team
* @version v1.0
*/
Abstract Class Initialize	{
	/**
    * protected $oBdd Database instance object
	* @var Object
    */
	protected $oBdd;
	/**
    * protected $GLOBALS_INI All globals from INI
	* @var Object
    */
	protected $GLOBALS_INI;
	/**
    * private $oForms Securite instance object
	* @var Object
	*/
	private $oForms;
	/**
    * public $VARS_HTML is used to store all datas get from View to send Controller (JSON)
	* @var Array
	*/
	public $VARS_HTML;
	/**
    * private $oForms Utils instance object
	* @var Object
	*/
	public $obj_utils;

	public function __construct()	{
		// Instance of Config
		$this->GLOBALS_INI= Configuration::getGlobalsINI();

		// Instance of BDD
		$this->oBdd = new Database($this->GLOBALS_INI["DB_HOST"],
								   $this->GLOBALS_INI["DB_NAME"],
								   $this->GLOBALS_INI["DB_LOGIN"],
								   $this->GLOBALS_INI["DB_PSW"]);

		// Instance of Securite to have $this->VARS_HTML
		$this->oForms= new Securite();
		$this->VARS_HTML= $this->oForms->VARS_HTML;

		$this->obj_utils = new Utils();
	}

	public function __destruct()	{
		// destroy Instance of Form
		unset($this->oForms);
		// disconnect of BDD
		// destroy Instance of BDD
		unset($this->oBdd);
		// destroy Instance of Utils
		unset($this->obj_utils);
	}
}
?>

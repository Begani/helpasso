<?php

require_once "initialize.php";


/**
* Class fiche_event_service | file fiche_event_service.php
*
* In this class, we have methods for :
* Function prepareEvent() which stock id_event selected by user in event_list
* Function loadEvent() using the getSelectDatas Method from database.php to get all datas of this id_event

*
* List of classes needed for this class : 
*
* require_once "initialize.php";
*
* @package HelpAsso Project
* @subpackage Initialize, Utils
* @author @HelpAsso - Benjamin Montet stagiaire
* @copyright  1920-2080 The Afpa Lab Team Group Corporation World Company
* @version v1.0
*/
class Fiche_event_service extends Initialize {

	/**
	* public $resultat is used to store all datas needed for HTML Templates
	* @var array
	*/public $resultat;
	
	/**
	* Call the parent constructor
	*
	* init variable resultat
	*/
	public function __construct() {
		// Call Parent Constructor
		parent::__construct();

		// init variables resultat
		$this->resultat = [];
	}
	/**
	* Call the parent destructor
	*/
	public function __destruct() {
		// Call Parent destructor
		parent::__destruct();
	}

	/**
	* This function stocks id_event selected by user in event_list
	*/
	public function prepareEvent(){

		if(!isset($this->VARS_HTML['eventSelected'])){
			error_log('erreur : id de l\'évènement inconnue');
		}
		else{
			$_SESSION["fiche_event"]= $this->VARS_HTML['eventSelected'];
			error_log($_SESSION["fiche_event"]);
		}
	}
	/**
	* This function uses the getSelectDatas Method from database.php to get all datas of this id_event
	*/
	public function loadEvent(){

		$spathSQL= $this->GLOBALS_INI["PATH_HOME"] . $this->GLOBALS_INI["PATH_SQL"] . "fiche_select_event.sql";
		$this->resultat["fiche_event"]= $this->oBdd->getSelectDatas($spathSQL, array(
			'id_event' => intVal($_SESSION["fiche_event"])
		));
	}
	

        

}

?>

<?php

require_once "initialize.php";
require_once "utils.php";

/**
* Class Event_service | file event_service.php
*
* In this class, we have methods to :
*
*  give the View contact 
*  onload : put datas from bdd to datalist (sport and city)
*
* List of classes needed for this class : 
*
* Initialize
* Utils
*
* @package Helpasso Project
* @subpackage initialize.php, utils.php
* @author @FansNHumans - Benjamin Montet stagiaire
* @copyright  2018-2020 The @Fansnhumans Team
* @version v1.0
*/
class Event_service extends Initialize {

    /**
    * public $resultat is used to store all datas needed for HTML Templates
    * @var Array
    */
    public $resultat;

    /**
    * Call the parent constructor
    *
    * init parameter resultat
    */
    public function __construct() {
        // Call Parent Constructor
        parent::__construct();

        // init parameter resultat
        $this->resultat = [];
    }
    /**
    * Call the parent destructor
    */
    public function __destruct() {
        // Call Parent destructor
        parent::__destruct();
    }


    /**
     * This method gives to the View all event in DB
     */
   

    public function getData(){

        $sqlPath = "tournoi_select_event.sql";

        if(isset($this->VARS_HTML['sql'])){

            switch($this->VARS_HTML['sql']){
                case 'ville':
                    $sqlPath = "tournoi_select_orderVille.sql";
                    error_log("ville");
                    break;
                case 'name_event': 
                    $sqlPath = "tournoi_select_orderName.sql";
                    error_log("nom_event");
                    break;
                case 'nom_contact': 
                    $sqlPath = "tournoi_select_orderContact.sql";
                    error_log("nom_event");
                    break;
                default  : 
                //Date begin ASC is defaut reqquest
                    $sqlPath = "tournoi_select_event.sql";
            }
        }

        $spathSQL= $this->GLOBALS_INI["PATH_HOME"] . $this->GLOBALS_INI["PATH_SQL"] . $sqlPath;
        $this->resultat["event"] = $this->oBdd->getSelectDatas($spathSQL, array());

    }

    public function construct_datalist(){
        $spathSQL= $this->GLOBALS_INI["PATH_HOME"] . $this->GLOBALS_INI["PATH_SQL"] . "admin_select_ville.sql";
        $this->resultat["admin_ville"]= $this->oBdd->getSelectDatas($spathSQL, array());

   
        $spathSQL= $this->GLOBALS_INI["PATH_HOME"] . $this->GLOBALS_INI["PATH_SQL"] . "admin_select_sport.sql";
        $this->resultat["admin_sport"]= $this->oBdd->getSelectDatas($spathSQL, array());

    }



}

?>

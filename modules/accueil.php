<?php

require_once "accueil_service.php";

/**
 * Class Accueil | fichier accueil.php
 *
 * Description de la classe à renseigner.
 *
 * In this class, we have 
 * 
 * private function main() : Instantiate class Accueil_service 
 * et passe les attributs resultat, VARS_HTML et LANG de la classe service à la classe métier
 *
 * List of classes needed for this class : 
 * Accueil_service
 *
 * @package Helpasso Project
 * @subpackage Accueil_service
 * @author @FansnHumans Team - Benjamin Montet Developper
 * @copyright  1920-2080 The FansnHumans Team
 * @version v1.0
 */
class Accueil {

	/**
     * public $resultat est utilisé pour stocker toutes les données nécessaires aux Templates HTML
     * @var array
     */
    public $resultat;

	/**
     * public $VARS_HTML est utilisé pour stocker toutes les données en POST et GET
     * @var array
     */
    public $VARS_HTML;

    /**
     * initialise la variable resultat
     * exécute la fonction main
     */
    public function __construct() {
        // init variables resultat
        $this->resultat = [];

        // execute main function
        $this->main();
    }

    /**
     * Détruit l'objet service
     */
    public function __destruct() {
        // destroy objet_service
        unset($objet_service);
    }

    /**
     * Initialise l'objet service Accueil_service 
     * et passe les attributs resultat, VARS_HTML de la classe service à la classe métier
     */
    public function main() {
		$objet_service = new Accueil_service();
		
		// Je passe mes paramètres pour y avoir accès dans mes pages HTML
		$this->resultat = $objet_service->resultat;
        $this->VARS_HTML = $objet_service->VARS_HTML;
    }
	
}

?>


<?php

require_once "admin_service.php";

/**
* Class Admin_delete | fichier admin_delete.php
*
* In this class, we have 
* Method main which instantiate Admin_service and call his method event_delete
 
* List of classes needed for this class :
* Admin_service
 
* @package Helpasso Project
* @subpackage admin_service.php
* @author @FansnHumans Team - Benjamin Montet Developper
* @copyright  2018-2020 The @Fansnhumans Team
* @version v1.0
*/
Class Admin_delete {

	/**
     * public $resultat is used to store all datas needed for HTML Templates
     * @var array
     */
    public $resultat;

    /**
     * init parameter resultat
     *
     * execute main method
     */
    public function __construct() {
        // init parameter resultat
        $this->resultat = [];

        // execute main method
        $this->main();
    }

    /**
     * Destroy service
     */
    public function __destruct() {
        // destroy objet_service
        unset($objet_service);
    }

    /**
    * Instantiate Class Admin_Service and execute his method event_delete
    */
    function main() {
		$objet_service = new Admin_service();
        $objet_service->event_delete();
        
		$this->resultat = $objet_service->resultat;
		$this->VARS_HTML = $objet_service->VARS_HTML;
    }
	
}

?>


<?php
require_once "service.php";
require_once "login_service.php";
/**
* Class login | fichier login.php
*
* Description de la classe à renseigner.
*
* Cette classe necessite l'utilisation de la classe :
*
* require_once "service.php";
*
* @package HelpAsso Project
* @subpackage service
* @author @Afpa Lab Team - Benjamin Montet stagiaire
* @copyright  1920-2080 The Afpa Lab Team Group Corporation World Company
* @version v1.0
*/

Class Login	{
	
	/**
	* public $resultat is used to store all datas needed for HTML Templates
	* @var array
	*/
	public $resultat;

	/**
	* init variables resultat
	*
	* execute main function
	*/
	public function __construct() {
		// init variables resultat
		$this->resultat = [];

		// execute main function
		$this->main();
	}

	/**
	*
	* Destroy service
	*
	*/
	public function __destruct() {
		// destroy objet_service
		unset($objet_service);
	}

	/**
	* Get interface to gestion of inscrit_admin
	*/
	private function main() {
		
		$objet_service = new Service();
		$objet_service = new Login_service();
		$this->resultat = $objet_service->resultat;
		$this->VARS_HTML = $objet_service->VARS_HTML;
		
		if(isset($this->VARS_HTML["mail_admin"]) && isset($this->VARS_HTML["mdp_admin"])){
			
			$res = $objet_service->login_select_admin();

			// foreach($res as $row){
			// 	error_log("foreach");
			// 	$id_admin = $row["id_admin"];
			// 	$mail_admin = $row["mail_admin"];
			// }
			error_log(count($res));

			if(count($res) == 1){
				
				$_SESSION['admin'] = true;
				// $this->resultat["sMessage"] = "Connexion réussie !";
				header('Location: admin');
			}
			else{
				$this->resultat["sMessage"] = "Erreur identifiant";
				$_SESSION['admin'] = false;
			}
		}
	}
}

?>


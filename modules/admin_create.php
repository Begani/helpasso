<?php
require_once "admin_service.php";
/**
* This Class is used to call event_create method 

* In this class, we have 

* Method main : instantiate class Admin_service and call event_create Method from Admin_service


*
* List of classes needed for this class : 
*
* Admin_service
* Service
*
* @package Helpasso Project
* @subpackage admin_service.php
* @author @HelpAsso - Benjamin Montet Developper
* @copyright  2018-2020 The @Fansnhumans Team
* @version v1.0
*/
class Admin_create	{
	
    /**
    * public $resultat is used to store all datas needed for HTML Templates
    * @var Array
    */
    public $resultat;

    /**
    * init parameter resultat
    *
    * execute main method
    */
    public function __construct()	{
        // init parameter resultat
        $this->resultat= [];

        // execute main method
        $this->main();
    }

    /**
    * Instantiate Class Admin_Service and execute his method event_create
    */
    function main() {
        $obj_inscription = new Admin_service();
        $obj_inscription->event_create();

        $this->resultat= $obj_inscription->resultat;
        $this->VARS_HTML= $obj_inscription->VARS_HTML;
    }
}

?>

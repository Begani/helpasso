<?php
require_once "service.php";
require_once "event_service.php";
/**
* Class Event_list | fichier event_list.php
*
* In this class, we have :
* Method main : instantiate classes Service and Event_service and call construct_datalist Method from Event_service

* List of classes needed for this class : 
* Event_service
* Service
*
* @package HelpAsso Project
* @subpackage service.php, event_service.php
* @author @Afpa Lab Team - Benjamin Montet stagiaire
* @copyright  1920-2080 The Afpa Lab Team Group Corporation World Company
* @version v1.0
*/

Class Event_list	{
	
    /**
    * public $resultat is used to store all datas needed for HTML Templates
    * @var array
    */
    public $resultat;

    /**
    * init parameter resultat
    *
    * execute main method
    */
    public function __construct() {
        // init parameter resultat
        $this->resultat = [];

        // execute main method
        $this->main();
    }

    /**
    *
    * Destroy service
    *
    */
    public function __destruct() {
        // destroy objet_service
        unset($objet_service);
    }


   function main() {
		
        $objet_service_tournoi = new Service();
        $objet_service_tournoi = new Event_service();

        $this->resultat = $objet_service_tournoi->resultat;
        $this->VARS_HTML = $objet_service_tournoi->VARS_HTML;
		
    
    }
}

?>


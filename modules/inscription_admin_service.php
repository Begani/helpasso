<?php

require_once "initialize.php";
require_once "utils.php";

/**
* Class inscription_admin_service | file inscription_admin_service.php
*
* In this class, we have methods for :
*
* With this interface, we'll be able to create new user
*
* List of classes needed for this class : 
*
* require_once "initialize.php";
* require_once "utils.php";
*
* @package HelpAsso Project
* @subpackage Initialize
* @author @Afpa Lab Team - Benjamin Montet stagiaire
* @copyright  1920-2080 The Afpa Lab Team Group Corporation World Company
* @version v1.0
*/
class Inscription_admin_service extends Initialize {

    /**
    * public $resultat is used to store all datas needed for HTML Templates
    * @var array
    */
    public $resultat;

    /**
    * Call the parent constructor
    *
    * init variables resultat
    */
    public function __construct() {
        // Call Parent Constructor
        parent::__construct();

        // init variables resultat
        $this->resultat = [];
    }

    /**
    * This function is used to register new admin in database
    */


    public function login_create_admin() {
        $spathSQL = $this->GLOBALS_INI["PATH_HOME"] . $this->GLOBALS_INI["PATH_SQL"] . "admin_create.sql";
        $this->resultat["inscription_admin"] = $this->oBdd->treatDatas($spathSQL, array(
                                                            "nom_admin" => $this->VARS_HTML["nom_admin"],
                                                            "prenom_admin" => $this->VARS_HTML["prenom_admin"],
                                                            "mail_admin" => $this->VARS_HTML["mail_admin"],
                                                            "mdp_admin" => $this->obj_utils->crypterssl($this->VARS_HTML["mdp_admin"])
                                                            ));
    }

    /**
    * Call the parent destructor
    */
    public function __destruct() {
        // Call Parent destructor
        parent::__destruct();
    }

}

?>

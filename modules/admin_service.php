<?php

require_once "initialize.php";
require_once "utils.php";

/**
* Class admin_service | file admin_service.php
*
* In this class, we have :
*
*  Methods to create, read, update or delete an event in DB
*  Method Execute SQL request with getSelectDatas Method to construct both datalist sport and city
*
* List of classes needed for this class : 
*
* Initialize;
* Utils;
*
* @package Helpasso Project
* @subpackage initialize.php, utils.php
* @author @FansNHumans - Benjamin Montet Developper
* @copyright  2018-2020 The @Fansnhumans Team
* @version v1.0
*/
Class Admin_service extends Initialize {

    /**
    * public $resultat is used to store all datas needed for HTML Templates
    * @var array
    */
    public $resultat;

    /**
    * Call the parent constructor
    *
    * init parameter resultat
    */
    public function __construct() {
        // Call Parent Constructor
        parent::__construct();

        // init parameter resultat
        $this->resultat = [];
    }
    /**
    * Call the parent destructor
    */
    public function __destruct() {
        // Call Parent destructor
        parent::__destruct();
    }

    /**
    * This method is used to get all cities in database to display them in datalist
    */
    public function construct_datalist(){
        $spathSQL= $this->GLOBALS_INI["PATH_HOME"] . $this->GLOBALS_INI["PATH_SQL"] . "admin_select_ville.sql";
        $this->resultat["admin_ville"]= $this->oBdd->getSelectDatas($spathSQL, array());

       
        $spathSQL= $this->GLOBALS_INI["PATH_HOME"] . $this->GLOBALS_INI["PATH_SQL"] . "admin_select_sport.sql";
        $this->resultat["admin_sport"] = $this->oBdd->getSelectDatas($spathSQL, array());

    }

    /**
     * This method is used to create new event in DB
     */
    public function event_create(){
        /**
         * This parameter is used to iterate over the while loop
         * @var Number
        */
        $i = 1;
        /**
         * This parameter reprensents the number of sport to bind to the new event
         * @var Number
        */
        $iNb_sport = $this->VARS_HTML["nb_sport"];
        /**
         * This parameter will contain the value of id_contact 
         * @var Number
        */
        $id_contact = 0;
        /**
         * This Array contains all datas of Contact sent by view
         * @var Array
        */
        $aOfContact = array( 
            "nom_contact" =>  $this->VARS_HTML["nom_contact"],
            "mail_contact" => $this->VARS_HTML["mail_contact"],
            "telephone_contact" => $this->VARS_HTML["telephone_contact"],
        );
      

        //  1.  Check if contact already exists in DB

        $spathSQL= $this->GLOBALS_INI["PATH_HOME"] . $this->GLOBALS_INI["PATH_SQL"] . "admin_select_contact.sql";
        $this->resultat["contact_exists"] = $this->oBdd->getSelectDatas($spathSQL, array(
             "mail_contact" => $this->VARS_HTML["mail_contact"]
        ));

        //If contact exists, then we store his id in $id_contact paramameter
        if(!empty($this->resultat["contact_exists"])){
            foreach($this->resultat["contact_exists"] as $row){
                $id_contact = $row["id_contact"];
            }
            error_log(print_r($this->resultat["contact_exists"],true));
            error_log('Le contact existe, son id est : ' . $id_contact);
            
        }
        //Otherwise, we insert it in BD and store the new contact id in the $id_contact paramameter
        else{
            $spathSQL= $this->GLOBALS_INI["PATH_HOME"] . $this->GLOBALS_INI["PATH_SQL"] . "admin_contact_insert.sql";
            $this->resultat["contact_insert"]= $this->oBdd->treatDatas($spathSQL, $aOfContact);
            $id_contact = $this->oBdd->getLastInsertId();
            
        }

       //  2.  Execute SQL request to create an event
        $spathSQL= $this->GLOBALS_INI["PATH_HOME"] . $this->GLOBALS_INI["PATH_SQL"] . "admin_create_event.sql";
        $this->resultat["create_event"]= $this->oBdd->treatDatas($spathSQL, array(
                                                                                    "name_event" => $this->VARS_HTML["name_event"], 
                                                                                    "nb_sport" => $iNb_sport,
                                                                                    "date_event_begin" => $this->VARS_HTML["date_event_begin"], 
                                                                                    "date_event_end" => $this->VARS_HTML["date_event_end"],
                                                                                    "adresse_event" => $this->VARS_HTML["adresse_event"],
                                                                                    "id_ville" => $this->VARS_HTML["ville_event"],
                                                                                    "id_contact" => $id_contact 
                                                                                ));
        $this->resultat["id_event_created"]= $this->oBdd->getLastInsertId();

        //  3.  Execute SQL request to bind all sports to this new event
        $spathSQL= $this->GLOBALS_INI["PATH_HOME"] . $this->GLOBALS_INI["PATH_SQL"] . "admin_bind_sport.sql";

        while ($i <= $iNb_sport ){
            $this->resultat["sport_binded"][$i] = $this->oBdd->treatDatas($spathSQL, array(
                                                                                            "id_sport" => $this->VARS_HTML["sport"][$i]['id_sport'],
                                                                                            "id_event" => $this->resultat["id_event_created"],
                                                                                            "date_sport_begin" => $this->VARS_HTML["sport"][$i]['date_sport_begin'],
                                                                                            "date_sport_end" => $this->VARS_HTML["sport"][$i]['date_sport_end'],
                                                                                            "details_sport" => $this->VARS_HTML["sport"][$i]['details_sport'],
                                                            ));
            $i++;   
            $this->resultat["id_event__sport"][$i]= $this->oBdd->getLastInsertId();
        }

    }



    /**
    * This method is used to store all event contained in DB
    */
    public function event_list() {
        $spathSQL= $this->GLOBALS_INI["PATH_HOME"] . $this->GLOBALS_INI["PATH_SQL"] . "admin_select_event_list.sql";
        $this->resultat["event_list"]= $this->oBdd->getSelectDatas($spathSQL, array());
    }

    /**
    * This method is used to update selected event
    */
    public function event_edit() {

        $spathSQL= $this->GLOBALS_INI["PATH_HOME"] . $this->GLOBALS_INI["PATH_SQL"] . "event_edit_enabled.sql";
        $this->resultat["event_edit_enabled"]= $this->oBdd->getSelectDatas($spathSQL, array(
                                                                                            "id_event" => $this->VARS_HTML["id_event"]
                                                                                            ));
    }


    /**
    * This method is used to delete selected event
    */
    public function event_delete() {
        $spathSQL= $this->GLOBALS_INI["PATH_HOME"] . $this->GLOBALS_INI["PATH_SQL"] . "admin_delete_event.sql";
        $this->resultat["event_delete"]= $this->oBdd->getSelectDatas($spathSQL, array(
                                                                                     "id_event" => $this->VARS_HTML["id_event"]
                                                                                    ));
    }

}


?>

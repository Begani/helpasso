
//Au chargement de la page, on éxécute les requetes pour alimenter les datalistes "ville" et "sport"
$(document).ready(function () {
    // loadVille();
    // loadSport();
    getListEvent();
    refreshView(1);
    $('#name_event').val("Tournoi");
    $('#adresse_event').val('Stade de Foot');
    //Penser a séparer la ville et le code postal

    $('#date_event_begin_hour').val("8h");
    $('#date_event_end_hour').val("17h");
    $('#nom_contact').val("Test");
    $('#mail_contact').val("testmail@test.fr");
    $('#telephone_contact').val("0251478963");
    $('#sport_begin_hour' + 1).val("9h");
    $('#sport_end_hour' +  1).val("17h");
    console.log('Loaded');
}); 



/**
* Initialisation des tableaux
* @param {!Array} aOfEvent - Liste des évènements
* @param {!Array} aOfVille - Liste des villes
* @param {!Array} aOfSport - Liste des sports
* @var tables - Configuration DataTables
*/

var tables;
let aOfVille = [];
let aOfSport = [];
let aOfEvent = [];



/**
* Fonction qui modifie les champs de saisie
* Appelle la fonction de modification  de la vue
*/
function testHide1Div(nb){
    // 1 : Récupération des données 
    nbMax = $('#nbInput').val();
    nb = parseInt(nb);

    while(nb < nbMax){
        //Recupere les info pour chaque div suivante celle supprimée
        //Place ces infos dans la div - 1
        next_data = nb + 1;
        console.log( "next_data : " + next_data);
        console.log( " entrée : " + $('#dl_sport-choix' + next_data).val());
        console.log("ce qu'il y a dans j c'est :" +  $("#dl_sport" + next_data + " option[value='" + $('#sport-choix' + next_data).val() + "']").val());

        $('#dl_sport-choix' + nb).val($("#dl_sport" + next_data + " option[value='" + $('#sport-choix' + next_data).val() + "']").val());
        $('#date_sport_begin' + nb).val("" +$('#date_sport_begin' + next_data).val());
        $('#date_sport_end' +  nb).val("" + $('#date_sport_end' + next_data).val());
        $('#sport_begin_hour' + nb).val("" +$('#sport_begin_hour' + next_data).val());
        $('#sport_end_hour' +  nb).val("" + $('#sport_end_hour' + next_data).val());
        $('#dl_sport-choix' +  nb).val("" + $('#dl_sport-choix' + next_data).val());
        
        nb++;
    }
    console.log('div que je ferme :' + nbMax);
    $('.hideMe' + nbMax).val("");

    //Now we get data from previous sport and moove them into new div
  
    
    removeSportView(nbMax);

  
}


// ------------------------ ENVOIE DES DONNEES DU FORMULAIRE AU PHP ------------------------------------ // 

/**
* Fonction qui permet l'envoie des données renseignées par l'admin pour la création de l'évènement en BDD
*/
function create_event(){
    $('#insModalSaving').show();
  
    /**
     * Initialisation des variables
     */
    aOfSport = [];
    var j = 1;
    var iSport = $('#nbInput').val();
    var details = "";
    //Séparation ville et CP
    let dataSplited = [];
    getVilleId = $("#dl_ville option[value='" + $('#ville-choix').val() + "']").attr('data-id');
    dataSplited = $('#dl_ville-choix').val().split(' - ');

    /**
    * Construction du tableau aOfSport pour envoyer en JSON
    */
    
    while( j <= iSport ){
       console.log('tour ' + j);
       //Recupère le nombre de jour pour chaque sport
       iNbJourSport = calculNbJour(j)

       //Organise les données de détails_event  pour chaque jour
       for(y = 1; y <= iNbJourSport; y++){
            details += "Jour " + y + ": " + $('#sport' + j +'_begin_hour_jour' + y).val() + " - " + $('#sport' + j +'_end_hour_jour' + y).val() + "Details : " + $('#details_sport'+ j +'_jour' + y).val() +" . \n"
       }
       


        aOfSport[j] = {
            id_sport : $("#dl_sport" + j + " option[value='" + $('#dl_sport-choix' + j).val() + "']").attr('data-id'),
            date_sport_begin : $('#date_sport_begin' + j).val(),
            date_sport_end : $('#date_sport_end' + j).val(),
            details_sport : details
        }

        console.log(aOfSport[j]);
        j++;   
    }


    /**
    * Verification des données du formulaire de création d'évènement.
    * 
    * console.log('data name_event : ' + $('#name_event').val());
    * console.log('data date_event_begin : ' + $('#date_event_begin').val());
    * console.log('data date_event_end: ' + $('#date_event_end').val());
    * console.log('data adresse_event: ' + $('#adresse_event').val());
    * console.log('data nom_contact: ' + $('#nom_contact').val());
    * console.log('data mail_contact: ' + $('#mail_contact').val());
    * console.log('data telephone_contact: ' + $('#telephone_contact').val());
    * console.log("data ville-event : " + $("#dl_ville option[value='" + $('#dl_ville-choix').val() + "']").attr('data-id'));
    * console.log("data sport choisi : " + $("#dl_sport" + 1 + " option[value='" + $('#dl_sport-choix' + 1).val() + "']").attr('data-id')) ;
    * 
    */
  


    /**
     * Création du tableau 'datas' de données à envoyer au pHp
     */
    
    var datas = {
        page : "admin_create",
        nb_sport : iSport,
        name_event : $('#name_event').val(),
        adresse_event : dataSplited[0],
        code_postal : dataSplited[1],
        ville_event : $("#dl_ville option[value='" + $('#dl_ville-choix').val() + "']").attr('data-id'),
        date_event_begin : $('#date_event_begin').val(),
        date_event_end : $('#date_event_end').val(),
        sport : aOfSport,
        nom_contact : $('#nom_contact').val(),
        mail_contact : $('#mail_contact').val(),
        telephone_contact : $('#telephone_contact').val(),
        bJSON : 1
    }
    console.log('test');
    $.ajax({
        type: "POST",
        url: "route.php",
        async: true,
        data: datas,
        dataType: "json",
        cache: false,
    })
    .done(function(aOfEvent) {
        window.location.href="admin"
    })
    .fail(function(err, result) {
        console.log(result);
        alert('error : ' + err);
    });

}



function getListEvent() {
    var datas = {
        bJSON: 1,
        page: "admin_list"
    }
    $.ajax({
        type: "POST",
        url: "route.php",
        async: true,
        data: datas,
        dataType: "json",
        cache: false,
    })
    .done(function(aOfEvent) {
        event_list = aOfEvent;
        constructTable(event_list);
        tables = $('#table_event').DataTable(configuration);
    })
    .fail(function(err) {
        console.log(err)
    });
}




/**
* Construie le datatable des évènements
* @param {!Array} aOfEvent - Liste des évènements
*/
function constructTable(aOfEvent) {
    var i;

    var sHTML = "";
    sHTML += "<thead>";
    sHTML += "<tr>";
    sHTML += "<th>ID</th>";
    sHTML += "<th>Nom de l'évenement</th>";
    sHTML += "<th>Date de début</th>";
    sHTML += "<th>Date de fin</th>";
    sHTML += "<th>Ville - Code postal</th>";
    sHTML += "<th>Adresse</th>";
    sHTML += "<th>Nom Contact</th>";
    sHTML += "<th>Détails</th>";
    sHTML += "<th>Editer</th>";
    sHTML += "</tr>";
    sHTML += "</thead>";

    sHTML += "<tbody>";

    for (i = 0; i < aOfEvent.length; i++) {

        sHTML += "<tr>";
        sHTML += "<td>" + aOfEvent[i]["id_event"] + "</td>";
        sHTML += "<td>" + aOfEvent[i]["nom_event"] + "</td>";
        sHTML += "<td>" + aOfEvent[i]["date_event_begin"];
        sHTML += "<td>" + aOfEvent[i]["date_event_end"];
        sHTML += "<td>" + aOfEvent[i]["ville"] + " </br> " + aOfEvent[i]["code_postal"] + "</td>";
        sHTML += "<td>" + aOfEvent[i]["adresse_event"] + "</td>";
        sHTML += "<td>" + aOfEvent[i]["nom_contact"] + "</td>";
        sHTML += '<td><button onclick="getPageDetails(\'details_event\',' + aOfEvent[i]["id_event"] + ')" class="btn btn-primary rounded-pill d-flex mx-auto">Détails</button></td>';
        sHTML += '<td><button onclick="edit_event(' + aOfEvent[i]["id_event"] + "," + i + ')" class="btn btn-primary rounded-pill d-flex mx-auto">Editer</button></td>';
        sHTML += "</tr>";
    }

    sHTML += "</tbody>";
    sHTML += "<tfoot>";
            sHTML += "<tr>";
            sHTML += "<th>ID</th>";
            sHTML += "<th>Nom de l'évenement</th>";
            sHTML += "<th>Date de début</th>";
            sHTML += "<th>Date de fin</th>";
            sHTML += "<th>Ville</th>";
            sHTML += "<th>Adresse</th>";
            sHTML += "<th>Nom Contact</th>";
            sHTML += "<th></th>";
            sHTML += "<th></th>";
        sHTML += "</tr>";
    sHTML += "</tfoot>";
    $('#table_event').html(sHTML);
    
}

// CONFIGURATION DATATABLE
var configuration = {
    "stateSave": false,
    "order": [
        [0, "asc"]
    ],
    "pagingType": "simple_numbers",
    "searching": true,
    "lengthMenu": [
        [5, 10, 25, -1],
        [5, 10, 25, "Tous"]
    ],
    "language": {
        "info": "Evènement _START_ à _END_ sur _TOTAL_ sélectionnés",
        "emptyTable": "Aucun évènements",
        "lengthMenu": "_MENU_ Evènements par page",
        "search": "Rechercher : ",
        "zeroRecords": "Aucun résultat de recherche",
        "paginate": {
            "previous": "Précédent",
            "next": "Suivant"
        },
        "sInfoFiltered": "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
        "sInfoEmpty": "Evènements 0 à 0 sur 0 sélectionnée",
    },
    "columns": [{ //ID
            "orderable": true,
            "responsivePriority": 1,
            
        },
        { // Nom de l'évenement
            "orderable": true,
            "responsivePriority": 1,
            
        },
        { // Date Début
            "orderable": true,
            "responsivePriority": 1,
            
        },
        { // Date Fin
            "orderable": true,
            "responsivePriority": 1,
            
        },
        { // Ville
            "orderable": true,
            "responsivePriority": 1,
            
        },
        { // Adresse
            "orderable": true,
            "responsivePriority": 1,
            
        },
        { // Nom contact
            "orderable": true,
            "responsivePriority": 1,
            
        },
       
        { // Détails
            "orderable": false,
            "responsivePriority": 1,
            
        },
        { // Editer
            "orderable": false,
            "responsivePriority": 1,
            
        }
    ],
};


// --------------------------------------- GESTION VUE --------------------------------------------- //
/**
* Fonction qui permet d'ajouter ou supprimer le nb de div active + envoie de ce nombre au pHp pour le nb de requete a executer
*/
function addSportView() {
    //BLoque a 10 le nb de sport associé a un évènement pour le moment
    if($('#nbInput').val() >= 10 ){
        return;
    }
    var nbInputToSend = parseInt($('#nbInput').val()) + 1;
    $('#nbInput').val(nbInputToSend);
    refreshView(nbInputToSend);

}

/**
* Fonction qui permet de vider les champs du sport précédemment supprimé
*/
function removeSportView(nbInputToSend) {
    //Si nb input est à 1 on vide les champs de la div_sport1
    if($('#nbInput').val() == 1){
        $('.hideMe' + 1).val("");
        return;
    }
    var nbInputToSend = nbInputToSend - 1;

    $('#nbInput').val(nbInputToSend);
    refreshView(nbInputToSend);

}

/**
* Fonction qui permet de cacher la div où le bouton supprimer a été cliqué
*/
function refreshView(iDivToShow){

    var i = 0;

    while ( i < iDivToShow){
        $('#main_sport'+ (i+1)).css('display', 'flex');
        i++;
    }

    while (i >= iDivToShow && i < 10){
        $('#main_sport'+ (i+1)).css('display', 'none');
        i++;
    }
}




//Cette fonction est appelée pour afficher une div pour chaque jour compris dans la durée de l'évènement
function calculNbJour(div){
    var getFirstDay = $('#date_sport_begin' + div).val();
    var getLastDay = $('#date_sport_end' + div).val();

    if(getFirstDay == "" || getLastDay == "" || getFirstDay > getLastDay){
        return;
    }
    firstDay = convertDate(getFirstDay).split('/');
    lastDay = convertDate(getLastDay).split('/');


    //Calcul du nombre de jour
   var iNbJour = parseInt(lastDay[0] - firstDay[0]) + 1;
   console.log(iNbJour);
   return iNbJour;
}

function displayNbJour(div){
    var i = 1;
    iNbJour = calculNbJour(div);
    //A chaque nouvel appel de la fonction on vide la div 
   if(iNbJour > 0){
       $('#detail_jours_sport' + div).html("");
   }
   
    while(i <= iNbJour){

        divToinject = `
        <div class="d-flex justify-content-betwwen align-items-center">
            <div class="col-lg-2">
                <h5>Jour ` + i +`</h5>
            </div> 
            <div class="col">
                <label for="sport`+ div +`_begin_hour_jour` + i +`" class="control-label">Heure début</label>
                <div class="col">
                    <input type="text" class="form-control hideMe` + i +`" name="sport`+ div +`_begin_hour_jour` + i +`" id="sport`+ div +`_begin_hour_jour` + i +`" >                
                </div>
            </div>
            <div class="col">
                <label for="sport`+ div +`_end_hour_jour` + i +`" class="control-label">Heure fin</label>
                <div class="col">
                    <input type="text" class="form-control hideMe` + i +`" name="sport`+ div +`_end_hour_jour` + i +`" id="sport`+ div +`_end_hour_jour` + i +`" >                
                </div>
            </div>   
            <div class="col">               
                <label for="details_sport`+ div +`_jour` + i +`" class="control-label">Détails</label>
                <div class="col">
                    <textarea class="form-control hideMe` + i +`" name="details_sport`+ div +`_jour` + i +`" id="details_sport`+ div +`_jour` + i +`" s></textarea>             
                </div>
            </div> 
        </div>
         `;
         $('#detail_jours_sport' + div).append(divToinject);

       i++;

   }
}
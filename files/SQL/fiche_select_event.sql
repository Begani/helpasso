Select event.id_event , sport.id_sport, nom_sport, date_sport_begin, date_sport_end, details_sport, event.id_contact, event.nom_event, ville.ville, ville.code_postal
from event__sport
INNER JOIN event on event.id_event = event__sport.id_event
INNER JOIN contact on contact.id_contact = event.id_contact
INNER JOIN ville on ville.id_ville = event.id_ville
INNER JOIN sport on sport.id_sport = event__sport.id_sport
WHERE event.id_event = @id_event
GROUP BY sport.id_sport
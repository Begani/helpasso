<?php

// List of classes needed for this class
require_once "logout_service.php";

Class Logout {
	
	public $resultat;

	public $VARS_HTML;

	public function __construct()	{
		$this->main();
	}

	    /**
     * Get interface to gestion of Stagiaires
     */
    function main() {
		$objet_service = new Logout_service();

		// Je passe mes paramètres pour y avoir accès dans mes pages HTML
		$this->resultat = $objet_service->resultat;
		$this->VARS_HTML = $objet_service->VARS_HTML;

		$_SESSION['admin'] = false;
		session_destroy();
		header('Location: ' . "accueil");
    }
}

?>

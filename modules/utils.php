<?php

Class Utils	{

	
	public function crypterssl($maChaineACrypter) {
		// Set a random salt
		// $salt = openssl_random_pseudo_bytes(8);
		// Or empty salt so that we'll be able to compare again
		$maCleDeCryptage= "1234567890";
		$salt= "";
		$salted = '';
		$dx = '';
		// Salt the key(32) and iv(16) = 48
		while (strlen($salted) < 48) {
		  $dx = md5($dx.$maCleDeCryptage.$salt, true);
		  $salted .= $dx;
		}
		$key = substr($salted, 0, 32);
		$iv  = substr($salted, 32,16);
		$encrypted_data = openssl_encrypt($maChaineACrypter, 'aes-256-cbc', $key, true, $iv);
		return base64_encode($salt . $encrypted_data);
	}

	public function html (string $str, $limit = false){
		if(is_int($limit) && strlen($str) > $limit) {
            $str = substr($str, 0, $limit).'...';
        }
        return nl2br(htmlentities($str));
	}
	
	
	public function prepareString($string) {
		
		$string = str_replace('œ', 'oe', $string);
		return preg_replace('/\W/', '-', trim(strtolower(str_replace(array(' ', "'", '"', '’'
			, 'Á', 'À', 'Â', 'Ä', 'Ã', 'Å'
			, 'Ç'
			, 'É', 'È', 'Ê', 'Ë'
			, 'Í', 'Ï', 'Î', 'Ì'
			, 'Ñ'
			, 'Ó', 'Ò', 'Ô', 'Ö', 'Õ'
			, 'Ú', 'Ù', 'Û', 'Ü'
			, 'Ý'
			, 'á', 'à', 'â', 'ä', 'ã', 'å', 'à'
			, 'ç'
			, 'é', 'è', 'ê', 'ë', 'é'
			, 'í', 'ì', 'î', 'ï'
			, 'ñ'
			, 'ó', 'ò', 'ô', 'ö', 'õ'
			, 'ú', 'ù', 'û', 'ü'
			, 'ý', 'ÿ'
			, '/', '!', '?', '«', '»', ' ', '.', '€', '%', ':', ',', "’")
			, array('-', '', '', ''
			, 'A', 'A', 'A', 'A', 'A', 'A'
			, 'C'
			, 'E', 'E', 'E', 'E'
			, 'I', 'I', 'I', 'I'
			, 'N'
			, 'O', 'O', 'O', 'O', 'O'
			, 'U', 'U', 'U', 'U'
			, 'Y'
			, 'a', 'a', 'a', 'a', 'a', 'a', 'a'
			, 'c'
			, 'e', 'e', 'e', 'e', 'e'
			, 'i', 'i', 'i', 'i'
			, 'n'
			, 'o', 'o', 'o', 'o', 'o'
			, 'u', 'u', 'u', 'u'
			, 'y', 'y'
			, '-', '', '', '', '', '', '', 'E', '', '', '', ''), utf8_decode(utf8_encode($string))))));
	}
	
	public function str_to_noaccent($str) {
		$url = $str;
		$url = preg_replace('#Ç#', 'C', $url);
		$url = preg_replace('#ç#', 'c', $url);
		$url = preg_replace('#è|é|ê|ë#', 'e', $url);
		$url = preg_replace('#È|É|Ê|Ë#', 'E', $url);
		$url = preg_replace('#à|á|â|ã|ä|å#', 'a', $url);
		$url = preg_replace('#@|À|Á|Â|Ã|Ä|Å#', 'A', $url);
		$url = preg_replace('#ì|í|î|ï#', 'i', $url);
		$url = preg_replace('#Ì|Í|Î|Ï#', 'I', $url);
		$url = preg_replace('#ð|ò|ó|ô|õ|ö#', 'o', $url);
		$url = preg_replace('#Ò|Ó|Ô|Õ|Ö#', 'O', $url);
		$url = preg_replace('#ù|ú|û|ü#', 'u', $url);
		$url = preg_replace('#Ù|Ú|Û|Ü#', 'U', $url);
		$url = preg_replace('#ý|ÿ#', 'y', $url);
		$url = preg_replace('#Ý#', 'Y', $url);
		
		return ($url);
	}
	
	
}


?>


var tables;
$(document).ready(function () 
{
    loadEvent(0);
    //Display Fullcalendar
   
    setView();
});

var aOfEvent = [];
var dateCall = false;
var villeCall = false;	
var nameCall = false;
var bPagination = false;


function loadEvent(btn)	{
    $('#insModalSaving').show();
    console.log(btn);
    console.log('test');
    var datas = {
        page : "event_list_recherche",
        sql : btn,
        bJSON : 1
    }
    $.ajax({
        type: "POST",
        url: "route.php",
        async: false,
        data: datas,
        dataType: "json",
        cache: false,
    })
    .done(function(result) {
        console.log(result);
        var iEvent= 0;

        //Gather Data from Json response
        for (var ligne in result)	{
            aOfEvent[iEvent]= [];
            aOfEvent[iEvent]["id_event"]= result[ligne]["id_event"];
            aOfEvent[iEvent]["url_img_event"]= result[ligne]["url_img_event"];
            aOfEvent[iEvent]["nom_event"]= result[ligne]["nom_event"];
            aOfEvent[iEvent]["date_event_begin"]= result[ligne]["date_event_begin"];
            aOfEvent[iEvent]["date_event_end"]= result[ligne]["date_event_end"];
            aOfEvent[iEvent]["nom_contact"]= result[ligne]["nom_contact"];
            aOfEvent[iEvent]["adresse_event"]= result[ligne]["adresse_event"];
            aOfEvent[iEvent]["code_postal"]= result[ligne]["code_postal"];
            aOfEvent[iEvent]["ville"]= result[ligne]["ville"];
            // $("td")
            // .find("[data-date='"+  aOfEvent[iEvent]["date_event_begin"] +"']").find('.fc-daygrid-day-events')
            // .html(
            //     `<a href="fiche_event" onclick="ficheEvent(` + aOfEvent[iEvent]["id_event"] + `)" >`+ aOfEvent[iEvent]["nom_event"]) + `</a>`
            iEvent++;
        }

        //Prepare data for FullCalendar events
        let evenements = [];
        for (y=0; y < aOfEvent.length; y++){
            evenements.push({
                "title" : aOfEvent[y]["nom_event"],
                "start" : aOfEvent[y]["date_event_begin"],
                "end" : aOfEvent[y]["date_event_end"]
        });
        };
        //Select where calendar goes
        var calendarEl = document.getElementById('calendar');
        //Instantiate FullCalendar Object and set configuration
        var calendar = new FullCalendar.Calendar(calendarEl, {
            initialView: 'dayGridMonth',
            events : evenements
        });
        calendar.render();
        
        $('#insModalSaving').hide();
        console.log(aOfEvent);
    })
    .fail(function(err, result) {
        console.log(result);
        alert('error : ' + err);
    });
}

function setView() {
    var i;
    
    var sHTML = "";

    for (i = 0; i < aOfEvent.length; i++) {
        sHTML += `
            <div class="card event_result search-item" id="event` + i +`" data-id="` + i +`" style="width: 18rem;">
                    <img class="card-img-top" src="` + aOfEvent[i]["url_img_event"] + `" alt="Card image cap">
                    <div class="card-body">
                        <h5 class="card-title">` + aOfEvent[i]["nom_event"] +`</h5>
                        <p> Date de début :` + aOfEvent[i]["date_event_begin"] +`</p>
                        <p> Date de fin : ` + aOfEvent[i]["date_event_end"] +`</p>
                        <p> adresse : ` + aOfEvent[i]["adresse_event"] +`</p>
                        <p> Ville : ` + aOfEvent[i]["ville"] + `</p>
                        <p> Code postal : ` + aOfEvent[i]["code_postal"] + `</p>
                        <p> Organisateur : ` + aOfEvent[i]["nom_contact"] +`</p>
                        <button onclick="ficheEvent(` + aOfEvent[i]["id_event"] + `)" class="btn btn-primary">Plus d'infos</button>
                    </div>
            </div>
        `;
    }
    
    $('.easyPaginateNav').remove();
    
    $('#paginate').html(sHTML).easyPaginate({
        paginateElement: $('.event_result'),
        elementsPerPage: 3,
        effect: 'climb'
    });


}


function dateChanger() {
    console.log(dateCall);
     
    if(dateCall == false){
        loadEvent('date');
        aOfEvent = aOfEvent.reverse();
        dateCall = true;
    }
    else{
        loadEvent('date');
        dateCall = false
    }
    setView();
}

function nameChanger() {
    console.log(nameCall);
    if(nameCall == false){
        loadEvent('name_event');
        nameCall = true;
    }
    else{
        loadEvent('name_event');
        aOfEvent = aOfEvent.reverse();
        nameCall = false;
    }
    setView();
    

}

function villeChanger() {
    console.log(villeCall);

    if(villeCall == false){
        loadEvent('ville');
        villeCall = true;
    }
    else{
        loadEvent('ville'); 
        aOfEvent = aOfEvent.reverse();
        villeCall = false;
    }
    console.log(aOfEvent);
    console.log(villeCall);
    setView();

}


function ficheEvent(id_event){
    var datas = {
        page : "fiche_event_selected",
        eventSelected : id_event,
        bJSON : 1
    }
    $.ajax({
        type: "POST",
        url: "route.php",
        async: true,
        data: datas,
        dataType: "json",
        cache: false,
    })
    .done(function(result) {
        console.log(result)
        window.location.href = "fiche_event";
    })
    .fail(function(err, result) {
        console.log(result);
        alert('error : ' + err);
    });
}


<?php

require_once "initialize.php";

/**
 * Class Accueil_service | file accueil_service.php
 *
 * In this class, we have methods for :
 *
 * Description of the class
 *
 * 
 * 
 * List of classes needed for this class
 * Initialize
 *
 * @package Helpasso Project
 * @subpackage Initialize
 * @author @FansnHumans Team - Franck Badji Developper
 * @copyright  1920-2080 The FansnHumans Team 
 * @version v1.0
 */
class Accueil_service extends Initialize {

	/**
     * public $resultat est utilisé pour stocker toutes les données nécessaires aux Templates HTML
     * @var array
     */
    public $resultat;

    /**
     * Appelle le constructeur parent
     * initialise l'array resultat
     */
    public function __construct() {
        // Appelle le constructeur parent (Initialize)
        parent::__construct();

        // initialise la l'array resultat
        $this->resultat = [];
    }

    /**
     * Appelle le destructeur parent
     */
    public function __destruct() {
        // Appelle le destructeur parent
        parent::__destruct();
    }

}

?>

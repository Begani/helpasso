<?php

require_once "initialize.php";

/**
 * Class Login_service | file login_service.php
 *
 * In this class, we have methods for :
 *
 * Description of the class
 *
 * List of classes needed for this class
 *
 * require_once "initialize.php";
 *
 * @package FansnHumans Project
 * @subpackage Login_service
 * @author @FansnHumans Team - Prenom Nom Developper
 * @copyright  1920-2080 The FansnHumans Team 
 * @version v1.0
 */
class Login_service extends Initialize {

    /**
     * public $resultat is used to store all datas needed for HTML Templates
     * @var array
     */
    public $resultat;

    /**
     * Call the parent constructor
     *
     * init variables resultat
     */
    public function __construct() {
        // Call Parent Constructor
        parent::__construct();

        // init variables resultat
        $this->resultat = [];
    }

    /**
     * Call the parent destructor
     */
    public function __destruct() {
        // Call Parent destructor
        parent::__destruct();
    }

    public function login_select_admin() {
        $spathSQL = $this->GLOBALS_INI["PATH_HOME"] . $this->GLOBALS_INI["PATH_SQL"] . "login_select_admin.sql";
        $res = $this->oBdd->getSelectDatas($spathSQL, array(
                                                            "mail_admin" => $this->VARS_HTML["mail_admin"],
                                                            "mdp_admin" => $this->obj_utils->crypterssl($this->VARS_HTML["mdp_admin"])
                                                            ));
                                                                
        print_r($res, true);
        return $res;
    }
}

?>

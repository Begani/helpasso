<?php

require_once "initialize.php";
require_once "utils.php";

/**
* Class admin_service | file admin_service.php
*
* In this class, we have methods for :
*
*  create, modify  or delete an event
*  onload : put datas from bdd to datalist (sport and city)
*
* List of classes needed for this class : 
*
* require_once "initialize.php";
* require_once "utils.php";
*
* @package helpAsso Project
* @subpackage Initialize
* @author @HelpAsso - Benjamin Montet stagiaire
* @copyright  1920-2080 The Afpa Lab Team Group Corporation World Company
* @version v1.0
*/
class Sendmail_service extends Initialize {

	/**
	* public $resultat is used to store all datas needed for HTML Templates
	* @var array
	*/public $resultat;
	
	/**
	* Call the parent constructor
	*
	* init variables resultat
	*/
	public function __construct() {
		// Call Parent Constructor
		parent::__construct();

		// init variables resultat
		$this->resultat = [];
	}
	/**
	* Call the parent destructor
	*/
	public function __destruct() {
		// Call Parent destructor
		parent::__destruct();
	}

	/**
	* This function is create new event in DB
	*/
	public function sendmail(){

		if(empty($this->VARS_HTML['nom'])){
			error_log("champ nom vide");
		}
		else if(empty($this->VARS_HTML['prenom'])){
			error_log("champ prenom vide");
		}
		else if(empty($this->VARS_HTML['mail'])){
			error_log("champ mail vide");
		}
		else if(empty($this->VARS_HTML['modal_message'])){
			error_log("champ modal_message vide");
		}
		else{
			error_log("Toutes les données ont été saisi, c'est parti pour la suite");
			$header="MIME-Version: 1.0\r\n";
			$header.='From:"MONTET "<benjam.montet@gmail.com>'."\n";
			$header.='Content-Type:text/html; charset="uft-8"'."\n";
			$header.='Content-Transfer-Encoding: 8bit';

			$nom = $this->VARS_HTML['nom'];
			$prenom =$this->VARS_HTML['prenom'];
			$mail = $this->VARS_HTML['mail'];
			$message_contact = $this->VARS_HTML['modal_message'];

			$message=`
			<html>
				<body>
					<div align="center">
						<u> Nom prénom de l'expéditeur :</u> ` . $nom . ` ` . $prenom . `
						<u> Mail de l'utilisateur :</u> ` . $mail . 
						`<br /> ` . nl2br($message_contact) .
						`<br />
					</div>
				</body>
			</html>
			`;
				
			error_log('Envoie du mail ');
			mail("helpassotest@gmail.com", "CONTACT - Helpasso.com", $message, $header);

			$this->resultat['mail_envoye'] = "Votre mail a bien été envoyé !";
		}

	}

        

}

?>

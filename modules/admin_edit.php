<?php

require_once "admin_service.php";

/**
 * Class Admin_edit | fichier admin_edit.php
 *
 * In this class, we have 
 * Method main which instantiate Admin_service and call his method event_edit
 
 * List of classes needed for this class :
 * Admin_service
 
 * @package Helpasso Project
 * @subpackage admin_service.php
 * @author @FansnHumans Team - Benjamin Montet Developper
 * @copyright  2018-2020 The @Fansnhumans Team
 * @version v1.0
 */
Class Admin_edit {

	/**
     * public $resultat is used to store all datas needed for HTML Templates
     * @var array
     */
    public $resultat;

	/**
     * public $VARS_HTML is used to store all datas POSTED and GETTED
     * @var array
     */
    public $VARS_HTML;

    /**
     * init parameter resultat
     *
     * execute main method
     */
    public function __construct() {
        // init parameter resultat
        $this->resultat = [];

        // execute main method
        $this->main();
    }

    /**
     *
     * Destroy service
     *
     */
    public function __destruct() {
        // destroy objet_service
        unset($objet_service);
    }

    /**
    * Instantiate Class Admin_Service and execute his method event_edit
    */
    function main() {
		$objet_service = new Admin_Service();

		$objet_service->event_edit();

		$this->resultat = $objet_service->resultat;
		$this->VARS_HTML = $objet_service->VARS_HTML;
    }
	
}

?>


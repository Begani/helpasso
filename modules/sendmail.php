<?php
require_once "service.php";
require_once "sendmail_service.php";
/**
* Class login | fichier login.php
*
* Description de la classe à renseigner.
*
* Cette classe necessite l'utilisation de la classe :
*
* require_once "service.php";
*
* @package HelpAsso Project
* @subpackage service
* @author @Afpa Lab Team - Benjamin Montet stagiaire
* @copyright  1920-2080 The Afpa Lab Team Group Corporation World Company
* @version v1.0
*/

Class Sendmail	{
	
	/**
	* public $resultat is used to store all datas needed for HTML Templates
	* @var array
	*/
	public $resultat;

	/**
	* init variables resultat
	*
	* execute main function
	*/
	public function __construct() {
		// init variables resultat
		$this->resultat = [];

		// execute main function
		$this->main();
	}

	/**
	*
	* Destroy service
	*
	*/
	public function __destruct() {
		// destroy objet_service
		unset($objet_service);
	}

	/**
	* Get interface to gestion of inscrit_admin
	*/
	public function main() {
		
		$objet_service = new Service();
		$objet_service = new Sendmail_service();

		$objet_service -> sendmail();
		$this->resultat = $objet_service->resultat;
		$this->VARS_HTML = $objet_service->VARS_HTML;
	}
		
}

?>

